# PART2
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans

df = pd.read_csv('C:/Users/matba/PycharmProjects/finalproject/Data/part1.csv')
print(df)


# CLUSTER QUESTION - We develop here the cluster

X = df[['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']].values
print('X.shape : ', X.shape)
kmeans = KMeans(n_clusters=4, random_state=42)
kmeans.fit(X)


# Labels - Here we're adding labels in the dataframe. 4 labels are randomly assigned (0,1,2,3).
labels = kmeans.labels_
print('labels.shape : ', labels.shape)
df['LABEL'] = labels
print(df)

# Cluster 3D display
fig = plt.figure(figsize=(20, 10))
ax = Axes3D(fig, auto_add_to_figure=False)
fig.add_axes(ax)
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=labels.astype(float))
ax.set_xlabel('FIRST_COURSE')
ax.set_ylabel('SECOND_COURSE')
ax.set_zlabel('THIRD_COURSE')
ax.set_title('')


# PART 3
# Q1 - Import part3 dataframe + Create column with true labels of client's type

df2 = pd.read_csv('C:/Users/matba/PycharmProjects/finalproject/Data/part3.csv')
print(df2)
df['TRUE_LABEL'] = df2['CLIENT_TYPE'].values
print(df)


# Q2 - Histogram with cluster's labels (0, 1, 2, 3)

df['LABEL'].hist(bins=np.linspace(0, 3))


# Q2 complement - Histogram with real Labels (Onetime, Retirement, Business, Healthy)

import matplotlib.pyplot as plt
plt.hist(df2['CLIENT_TYPE'], color = 'blue')


# Q3 - Likelihood for a type of client to get a certain course (First, Second or Third)

for label in df['TRUE_LABEL'].unique():
    for course in ['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']:
        emp_mean = np.mean((df.loc[df['TRUE_LABEL'] == label, course] != 0).values)
        print('Likelihood for client type {} to get the {} : {}'.format(label, course, emp_mean))


# Q4 - Probability for a certain type to eat a certain dish

import numpy as np
df_Retirement = df.loc[df['TRUE_LABEL']=="Retirement",:]
dessert_rtrmt = df_Retirement.loc[df_Retirement['THIRD_COURSE']!=0,:]
conditionlist = [
    (dessert_rtrmt['THIRD_COURSE'] < 15),
    (dessert_rtrmt['THIRD_COURSE'] >= 15)]
choicelist = ['Pie', 'Ice-Cream']
df_Retirement['DESSERT'] = np.select(conditionlist, choicelist)
print(df_Retirement)

df_Retirement['CLIENT_ID'].count()
pie = df_Retirement.loc[df_Retirement['DESSERT']=='Pie',:]
pie['CLIENT_ID'].count()
RtrmteatPie = (pie['CLIENT_ID'].count())/(df_Retirement['CLIENT_ID'].count())
print('Likelihood for client type Retirement to eat a pie is ', RtrmteatPie)


# Q5 - Determine the distribution of dishes, per course, per customer type

def determine_starter(x):
    if x == 0:
        return None
    elif x > 20:
        return 'Oysters'
    elif x > 15:
        return 'Tomato-Mozarella'
    else:
        return 'Soup'

def determine_main(x):
    if x == 0:
        return None
    elif x > 40:
        return 'Labster'
    elif x > 25:
        return 'Steak'
    elif x > 20:
        return 'Spaghetti'
    else:
        return 'Salad'

def determine_dessert(x):
    if x == 0:
        return None
    elif x > 15:
        return 'Pie'
    else:
        return 'Ice_cream'

df = (
    df
    .copy()
    .assign(**{
        'STARTER': lambda x: x['FIRST_COURSE'].apply(determine_starter),
        'MAIN': lambda x: x['SECOND_COURSE'].apply(determine_main),
        'DESSERT': lambda x: x['THIRD_COURSE'].apply(determine_dessert)
    })
)
print(df)

fig, axs = plt.subplots(df['LABEL'].unique().shape[0], 3, figsize=(20, 17))
for customer_type in df['LABEL'].unique():
    for i, course in enumerate(['STARTER', 'MAIN', 'DESSERT']):
        axs[customer_type, i].hist(df.loc[(df['LABEL'] == customer_type) & (~df[course].isnull()), course])
        axs[customer_type, i].set_title(f'Customer type {customer_type} for the course {course}')

def determine_starter_drink(x):
    if x == 0:
        return None
    elif x > 20:
        return x - 20
    elif x > 15:
        return x - 15
    else:
        return x - 3

def determine_main_drink(x):
    if x == 0:
        return None
    elif x > 40:
        return x - 40
    elif x > 25:
        return x - 25
    elif x > 20:
        return x - 20
    else:
        return x - 9

def determine_dessert_drink(x):
    if x == 0:
        return None
    elif x > 15:
        return x - 15
    else:
        return x - 10

df = (
    df
    .copy()
    .assign(**{
        'STARTER_DRINK': lambda x: x['FIRST_COURSE'].apply(determine_starter_drink),
        'MAIN_DRINK': lambda x: x['SECOND_COURSE'].apply(determine_main_drink),
        'DESSERT_DRINK': lambda x: x['THIRD_COURSE'].apply(determine_dessert_drink)
    })
)
print(df)


# Q6 - Distribution of the cost of the drinks per course

fig, axs = plt.subplots(3, 1, figsize=(8, 15))
for i, course in enumerate(['STARTER', 'MAIN', 'DESSERT']):
    axs[i].hist(df.loc[(~df[course].isnull()), course+'_DRINK'])
    axs[i].set_title(course+'_DRINK')





# Part4 - Restaurant simulation

import time
from datetime import datetime
import numpy as np
from os.path import exists
import pandas as pd
import random

# Q1 - Class the client for the simulation
class Gen_Client:
    type = ""
    starter = ["Soup", "Tomato-Mozarella", "Oysters"]
    main = ["Salad", "Spaghetti", "Steak", "Lobster"]
    dessert = ["Ice cream", "Pie"]
    choice1 = ""
    choice2 = ""
    choice3 = ""
    idList = []
    id = ""

    # Check if the client exists or generate a new one and its type.
    def __init__(self, c_id, Newid_df):
        new_client = True
        print(Newid_df)
        for client in Newid_df:
            if c_id == client:
                new_client = False
        if new_client:
            c_type = input("Which type of client are you?")
            self.idList.append(c_id)
            self.id = c_id
            self.type = c_type

# Start of the simulation
    def goToRestaurant(self):
        print("We're pleased to meet you in our Restaurant and her is our menu:\n\t"
              "Starters:\n\t\t*" + self.starter[0] + ":1\n\t\t*" + self.starter[1] + ":2\n\t\t*" +
              self.starter[2] +
              ":3\n\t"
              "Mains:\n\t\t*" + self.main[0] + ":1\n\t\t*" + self.main[1] + ":2\n\t\t*" +
              self.main[2] +
              ":3\n\t\t*" + self.main[3] + ":4\n\t"
            "Desserts:\n\t\t*" + self.dessert[0] + ":1\n\t\t*" +
              self.dessert[1] + ":2")

        # Select the starter
        print("\nWhat would you take as starter? O for nothing.")

        # Check if the input is correct
        is_int = False
        while not is_int:
            try:
                print("\n\t\t*Nothing:0\n\t\t*" + self.starter[0] + ":1\n\t\t*" + self.starter[1]
                      + ":2\n\t\t*" + self.starter[2] + ":3")
                order1 = int(input("enter number here:"))
                is_int = True
                if order1 == 0:
                    self.choice1 = "none"
                    print("Your choice: " + self.choice1)
                elif order1 == 1:
                    self.choice1 = self.starter[0]
                    print("Your choice: " + self.choice1)
                elif order1 == 2:
                    self.choice1 = self.starter[1]
                    print("Your choice: " + self.choice1)
                elif order1 == 3:
                    self.choice1 = self.starter[2]
                    print("Your choice: " + self.choice1)
                else:
                    print("\nPlease enter a number between 0 and 3")
                    is_int = False
            except ValueError:
                print("\nPlease enter a number between 0 and 3")

                print("\n\t\t*Nothing:0\n\t\t*" + self.starter[0] + ":1\n\t\t*" + self.starter[1]
                      + ":2\n\t\t*" + self.starter[2] + ":3")
                is_int = False

        # Same operation for main
        print("\nWhat would you take as Main? 0 for nothing.")
        is_int = False
        while not is_int:
            try:
                order2 = int(input("enter number here:"))
                print("\n\t\t*Nothing:0\n\t\t*" + self.main[0] + ":1\n\t\t*" + self.main[1] +
                      ":2\n\t\t*" + self.main[2] + ":3\n\t\t*" + self.main[3] + ":4")
                is_int = True
                if order2 == 0:
                    self.choice2 = "none"
                    print("Your choice: " + self.choice2)
                elif order2 == 1:
                    self.choice2 = self.main[0]
                    print("Your choice: " + self.choice2)
                elif order2 == 2:
                    self.choice2 = self.main[1]
                    print("Your choice: " + self.choice2)
                elif order2 == 3:
                    self.choice2 = self.main[2]
                    print("Your choice: " + self.choice2)
                elif order2 == 4:
                    self.choice2 = self.main[3]
                    print("Your choice: " + self.choice2)
                else:
                    print("\nPlease enter a number between 0 and 4")
                    is_int = False
            except ValueError:
                print("\nPlease enter a number between 0 and 4")
                print("\n\t\t*Nothing:0\n\t\t*" + self.main[0] + ":1\n\t\t*" + self.main[1] +
                      ":2\n\t\t*" + self.main[2] + ":3\n\t\t*" + self.main[3] + ":4")
                is_int = False

        # Same operation for dessert
        print("\nWhat would you take as Dessert? O for nothing.")

        is_int = False
        while not is_int:
            try:
                print("\n\t\t*Nothing:0\n\t\t*" + self.dessert[0] + ":1\n\t\t*" + self.dessert[1] +
                      ":2")
                order3 = int(input("enter number here:"))
                is_int = True
                if order3 == 0:
                    self.choice3 = "none"
                    print("Your choice: " + self.choice3)
                elif order3 == 1:
                    self.choice3 = self.dessert[0]
                    print("Your choice: " + self.choice3)
                elif order3 == 2:
                    self.choice3 = self.dessert[1]
                    print("Your choice: " + self.choice3)
                else:
                    print("\nPlease enter a number between 0 and 2")
                    is_int = False
            except ValueError:
                print("\nPlease enter a number between 0 and 2")
                is_int = False

        # Define the current time
        timestamp = time.time()
        # Convert into datetime
        date_time = datetime.fromtimestamp(timestamp)
        # Convert timestamp to string in format: dd-mm-yyyy and HH:MM:SS
        str_date_time = date_time.strftime("%d-%m-%Y, %H:%M:%S")
        # Create the object
        df_final = save_Courses(self.choice1, self.choice2, self.choice3, str_date_time, self.id)

        # Create or add the orders to csv file
        if exists("C:/Users/matba/PycharmProjects/finalproject/Data/simulation.csv"):
            df_old = pd.read_csv("C:/Users/matba/PycharmProjects/finalproject/Data/simulation.csv", index_col=0)
            df_old.loc[len(df_old.index)] = [self.id, self.choice1, self.choice2, self.choice3, str_date_time]
            df_old.to_csv("C:/Users/matba/PycharmProjects/finalproject/Data/simulation.csv", index=True)
        else:
            df_final.df_new.to_csv("C:/Users/matba/PycharmProjects/finalproject/Data/simulation.csv")

class save_Courses:
    first_order = []
    second_order = []
    third_order = []
    c_id = []
    order_time = []
    df_new = pd.DataFrame()

    def __init__(self, starter, main, dessert, time_of_order, cli_id):
        self.first_order.append(starter)
        self.second_order.append(main)
        self.third_order.append(dessert)
        self.order_time.append(time_of_order)
        self.c_id.append(cli_id)
        self.updateDataFrame()

    def updateDataFrame(self):
        df_id = np.array(self.c_id, dtype='object')
        self.df_new["id"] = df_id.tolist()

        df_course1 = np.array(self.first_order, dtype='object')
        self.df_new["Starter"] = df_course1.tolist()

        df_course2 = np.array(self.second_order, dtype='object')
        self.df_new["Main"] = df_course2.tolist()

        df_course3 = np.array(self.third_order, dtype='object')
        self.df_new["Dessert"] = df_course3.tolist()

        df_time = np.array(self.order_time, dtype='object')
        self.df_new["Time"] = df_time.tolist()

# Define a random ID for the customer and control if this ID already exist
def main():
    ID_Customers = 'cl' + str(random.randrange(100000, 999999))
    if exists("C:/Users/matba/PycharmProjects/finalproject/Data/simulation.csv"):
        df = pd.read_csv("C:/Users/matba/PycharmProjects/finalproject/Data/simulation.csv")
        Client = Gen_Client(ID_Customers, df['id'])
        Client.goToRestaurant()
    else:
        idd = []
        Client = Gen_Client(ID_Customers, idd)
        Client.goToRestaurant()
if __name__ == '__main__':
    main()