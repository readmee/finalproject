#Q1 - Import the database

import pandas as pd
df = pd.read_csv('C:/Users/matba/PycharmProjects/finalproject/Data/part1.csv')
df.to_csv('myDataFrame.csv')
print(df)
print('myDataFrame.csv')


# Q2 - Distribution plot of the cost of each course
# Run each distribution plot individually in the python console for a clearer outcome

import matplotlib.pyplot as plt
import seaborn as sns

sns.distplot(df['FIRST_COURSE'], hist=True, kde=True,
             bins=int(25/1), color = 'red')
plt.show()

sns.distplot(df['SECOND_COURSE'], hist=True, kde=True,
             bins=int(50/1), color = 'green')
plt.show()
sns.distplot(df['THIRD_COURSE'], hist=True, kde=True,
             bins=int(15/1), color = 'blue')
plt.show()


# Q3 - Barplot display

import pandas as pd
import numpy as np
sum_starters = df['FIRST_COURSE'].sum()
sum_means = df['SECOND_COURSE'].sum()
sum_desserts = df['THIRD_COURSE'].sum()

barplot = pd.DataFrame({'courses':['FIRST_COURSE','SECOND_COURSE','THIRD_COURSE'],
                        'Expenditure':[sum_starters,sum_means,sum_desserts]})
graph = barplot.plot.bar(x ='courses', y='Expenditure' ,rot=0)


# Q4 - Determine the cost of each drinks per course
# We first started to create 2 columns with the couse's name and it's price and then substract the price of the
# course's cost to the total course's cost.

# Determine the price of the Starter drink.
new_df = df.assign(Starter='Plat')
conditionlist = [
    (new_df['FIRST_COURSE'] >= 3) & (new_df['FIRST_COURSE'] < 15),
    (new_df['FIRST_COURSE'] >= 15) & (new_df['FIRST_COURSE'] < 20),
    (new_df['FIRST_COURSE'] >= 20)]
choicelist = ['Soup', 'Tomato-Mozza', 'Oysters']
choicelist1 = [float(3), float(15), float(20)]
new_df['Starter'] = np.select(conditionlist, choicelist, default= 'Nothing')
new_df['StarterCost'] = np.select(conditionlist, choicelist1, default= '0')

new_df['FIRST_COURSE'].apply(lambda x: float(x))
new_df['StarterCost']= new_df['StarterCost'].astype('float')
new_df = new_df.assign(Drink_Price= new_df['FIRST_COURSE'] - new_df['StarterCost'])
print(new_df)

# Determine the price of the Main drink.
conditionlist = [
    (new_df['SECOND_COURSE'] >= 9) & (new_df['SECOND_COURSE'] < 20),
    (new_df['SECOND_COURSE'] >= 20) & (new_df['SECOND_COURSE'] < 25),
    (new_df['SECOND_COURSE'] >= 25) & (new_df['SECOND_COURSE'] < 40),
    (new_df['SECOND_COURSE'] >= 40)]
choicelist = ['Soup', 'Spaghetti', 'Steak','Lobster']
choicelist1 = [float(9), float(20), float(25),float(40)]
new_df['Main'] = np.select(conditionlist, choicelist, default= 'Nothing')
new_df['MainCost'] = np.select(conditionlist, choicelist1, default= '0')

new_df['SECOND_COURSE'].apply(lambda x: float(x))
new_df['MainCost']= new_df['MainCost'].astype('float')
new_df = new_df.assign(Drink_Price= new_df['SECOND_COURSE'] - new_df['MainCost'])
print(new_df)

# Determine the price of the Dessert drink.
conditionlist = [
    (new_df['THIRD_COURSE'] >= 10) & (new_df['THIRD_COURSE'] < 15),
    (new_df['THIRD_COURSE'] >= 15)]
choicelist = ['Pie', 'Ice-Cream']
choicelist1 = [float(10), float(15)]
new_df['Dessert'] = np.select(conditionlist, choicelist, default= 'Nothing')
new_df['DessertCost'] = np.select(conditionlist, choicelist1, default= '0')

new_df['THIRD_COURSE'].apply(lambda x: float(x))
new_df['DessertCost']= new_df['DessertCost'].astype('float')
new_df = new_df.assign(Drink_Price= new_df['THIRD_COURSE'] - new_df['DessertCost'])
print(new_df)
